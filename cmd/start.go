/*
Copyright © 2021 NAME HERE hello@long-bui.de

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/peterbourgon/diskv/v3"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Starts the pinboi bot",
	Long:  `One and only command to start the pinboi bot`,
	Run:   runStart,
}

func init() {
	rootCmd.AddCommand(startCmd)
}

var storage = diskv.New(diskv.Options{
	BasePath:     "pinboi-data",
	Transform:    func(s string) []string { return []string{} },
	CacheSizeMax: 1024 * 1024,
})

const pinboi_prefix = "!pinboi"
const pinboi_list_prefix = "!pinboi list"

func runStart(cmd *cobra.Command, args []string) {
	log.Printf("Initialize Bot")
	discord, err := discordgo.New("Bot " + viper.GetString("TOKEN"))
	if err != nil {
		panic(err)
	}
	sigintChan := make(chan os.Signal, 1)
	signal.Notify(sigintChan, syscall.SIGINT)
	discord.AddHandler(handleText)
	log.Printf("Open connection to discord")
	err = discord.Open()
	defer discord.Close()
	if err != nil {
		panic(err)
	}
	s := <-sigintChan
	log.Printf("Exiting due to Signal %v", s)
}

func handleText(discord *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == discord.State.User.ID {
		return
	}
	if strings.HasPrefix(m.Content, pinboi_list_prefix) {
		keys := storage.Keys(nil)
		messageBuilder := strings.Builder{}
		var start = true
		discord.ChannelMessageSend(m.ChannelID, "Registered Pins")
		for key := range keys {
			if start {
				start = false
			} else {
				messageBuilder.WriteString("\n")
			}
			messageBuilder.WriteString(key)
		}
		discord.ChannelMessageSend(m.ChannelID, messageBuilder.String())
		return
	}
	if strings.HasPrefix(m.Content, pinboi_prefix) {
		strippedString := strings.Split(m.Content[len(pinboi_prefix)+1:], " ")
		if len(strippedString) < 2 {
			log.Println("Not a valid command")
			return
		}
		registerPin(strippedString[0], strings.Join(strippedString[1:], " "))
		return
	}
	if strings.HasPrefix(m.Content, "!") && storage.Has(m.Content[1:]) {
		var value, err = storage.Read(m.Content[1:])
		if err != nil {
			log.Println("Not found!")
		}
		discord.ChannelMessageSend(m.ChannelID, string(value))
	}
}

func registerPin(alias, content string) {
	storage.Write(alias, []byte(content))
}
