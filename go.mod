module github.com/kasokz/pinboi-bot

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/peterbourgon/diskv/v3 v3.0.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
)
